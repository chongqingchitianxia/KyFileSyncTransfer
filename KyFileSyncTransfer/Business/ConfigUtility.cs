﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
﻿using System.Configuration;

namespace KyFileSyncTransfer.Business
{

    public static class ConfigUtility
    {

        /// <summary>
        /// 获取配置值
        /// </summary>
        public static string GetAppSettingValue(string key, string defaultValue = null)
        {
            string settingValue = ConfigurationManager.AppSettings[key];
            if (settingValue == null)
            {
                return defaultValue;
            }
            return settingValue;
        }

        /// <summary>
        /// 设置配置值（存在则更新，不存在则新增）
        /// </summary>
        public static void SetAppSettingValue(string key, string value)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var setting = config.AppSettings.Settings[key];
            if (setting == null)
            {
                config.AppSettings.Settings.Add(key, value);
            }
            else
            {
                setting.Value = value;
            }

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// 删除配置值
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveAppSetting(string key)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings.Remove(key);
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }


        /// <summary>
        /// 设置多个配置值（存在则更新，不存在则新增）
        /// </summary>
        /// <param name="settingValues"></param>
        public static void SetAppSettingValues(IEnumerable<KeyValuePair<string, string>> settingValues)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            foreach (var item in settingValues)
            {
                var setting = config.AppSettings.Settings[item.Key];
                if (setting == null)
                {
                    config.AppSettings.Settings.Add(item.Key, item.Value);
                }
                else
                {
                    setting.Value = item.Value;
                }
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// 获取所有配置值
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetAppSettingValues()
        {
            Dictionary<string, string> settingDic = new Dictionary<string, string>();
            var settings = ConfigurationManager.AppSettings;
            foreach (string key in settings.Keys)
            {
                settingDic[key] = settings[key].ToString();
            }
            return settingDic;
        }

        /// <summary>
        /// 删除多个配置值
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveAppSettings(params string[] keys)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (keys != null)
            {
                foreach (string key in keys)
                {
                    config.AppSettings.Settings.Remove(key);
                }
            }
            else
            {
                config.AppSettings.Settings.Clear();
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// 获取连接字符串
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetConnectionString(string name, string defaultconnStr = null)
        {
            var connStrSettings = ConfigurationManager.ConnectionStrings[name];
            if (connStrSettings == null)
            {
                return defaultconnStr;
            }
            return connStrSettings.ConnectionString;
        }

        /// <summary>
        /// 设置连接字符串的值（存在则更新，不存在则新增）
        /// </summary>
        /// <param name="name"></param>
        /// <param name="connstr"></param>
        /// <param name="provider"></param>
        public static void SetConnectionString(string name, string connstr, string provider)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConnectionStringSettings connStrSettings = config.ConnectionStrings.ConnectionStrings[name];
            if (connStrSettings != null)
            {
                connStrSettings.ConnectionString = connstr;
                connStrSettings.ProviderName = provider;
            }
            else
            {
                connStrSettings = new ConnectionStringSettings(name, connstr, provider);
                config.ConnectionStrings.ConnectionStrings.Add(connStrSettings);
            }

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("connectionStrings");
        }

        /// <summary>
        /// 删除连接字符串配置项
        /// </summary>
        /// <param name="name"></param>
        public static void RemoveConnectionString(string name)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings.Remove(name);
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("connectionStrings");
        }

        /// <summary>
        /// 获取所有的连接字符串
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetConnectionStrings()
        {
            Dictionary<string, string> connStrSettingDic = new Dictionary<string, string>();
            var connStrSettings = ConfigurationManager.ConnectionStrings;
            foreach (ConnectionStringSettings item in connStrSettings)
            {
                connStrSettingDic[item.Name] = item.ConnectionString;
            }
            return connStrSettingDic;
        }

        /// <summary>
        /// 设置多个连接字符串的值（存在则更新，不存在则新增）
        /// </summary>
        /// <param name="nameAndConnStrDic"></param>
        /// <param name="nameAndProviderDic"></param>
        public static void SetConnectionStrings(Dictionary<string, string> nameAndConnStrDic, Dictionary<string, string> nameAndProviderDic)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            foreach (string key in nameAndConnStrDic.Keys)
            {
                ConnectionStringSettings connStrSettings = config.ConnectionStrings.ConnectionStrings[key];
                if (connStrSettings != null)
                {
                    connStrSettings.ConnectionString = nameAndConnStrDic[key];
                    connStrSettings.ProviderName = nameAndProviderDic[key];
                }
                else
                {
                    connStrSettings = new ConnectionStringSettings(key, nameAndConnStrDic[key], nameAndProviderDic[key]);
                    config.ConnectionStrings.ConnectionStrings.Add(connStrSettings);
                }
            }

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("connectionStrings");
        }

        /// <summary>
        /// 删除多个连接字符串配置项
        /// </summary>
        /// <param name="names"></param>
        public static void RemoveConnectionStrings(params string[] names)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (names != null)
            {
                foreach (string name in names)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove(name);
                }
            }
            else
            {
                config.ConnectionStrings.ConnectionStrings.Clear();
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("connectionStrings");
        }



    }


}
